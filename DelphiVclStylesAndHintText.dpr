program DelphiVclStylesAndHintText;

uses
  Vcl.Forms,
  DelphiVclStylesAndHintTextFormUnit in 'DelphiVclStylesAndHintTextFormUnit.pas' {DelphiVclStylesAndHintTextForm},
  Vcl.Themes,
  Vcl.Styles,
  WinControlTooltipUnit in 'WinControlTooltipUnit.pas',
  Patch.Vcl.Themes.GetSysWindowText in 'Patch.Vcl.Themes.GetSysWindowText.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TDelphiVclStylesAndHintTextForm, DelphiVclStylesAndHintTextForm);
  Application.Run;
end.
