unit Patch.Vcl.Themes.GetSysWindowText;

interface

implementation

{$INCLUDE jedi\JEDI.inc}

{$IFDEF DELPHIXE6_UP}
  {$IFDEF DELPHIX_TOKYO_UP}
    {$DEFINE Skip_Patch_Vcl_Themes_GetSysWindowText}
  {$ENDIF DELPHIXE6_UP}
{$ELSE}
  {$DEFINE Skip_Patch_Vcl_Themes_GetSysWindowText}
{$ENDIF DELPHIXE6_UP}

{$IFNDEF Skip_Patch_Vcl_Themes_GetSysWindowText}
uses
  System.Rtti,
  System.SysUtils,
  Vcl.Themes,
  Winapi.Messages,
  Winapi.Windows;

// Two ways of doing this: using GetWindowText or by sending a WM_GETTEXT message
// https://blogs.msdn.microsoft.com/oldnewthing/20030821-00/?p=42833 explains the difference and favours WM_GETTEXT
// - GetWindowText: https://msdn.microsoft.com/en-us/library/windows/desktop/ms633520.aspx
// - WM_GETTEXT: https://msdn.microsoft.com/en-us/library/windows/desktop/ms632627.aspx

function GetSysWindowText_GetWindowText(Window: HWND): string;
var
  RequiredTextLength: Integer;
  ObtainedTextLength: Integer;
begin
  RequiredTextLength := SendMessage(Window, WM_GETTEXTLENGTH, 0, 0);
  SetString(Result, PChar(nil), RequiredTextLength);
  if RequiredTextLength <> 0 then
  begin
    ObtainedTextLength := Winapi.Windows.GetWindowText(Window, PChar(Result), RequiredTextLength);
    if ObtainedTextLength < RequiredTextLength then
      SetLength(Result, ObtainedTextLength);
  end;
end;

function GetSysWindowText_WMGetText(Window: HWND): string;
var
  RequiredTextLength: Integer;
  ObtainedTextLength: Integer;
begin
  RequiredTextLength := SendMessage(Window, WM_GETTEXTLENGTH, 0, 0);
  SetString(Result, PChar(nil), RequiredTextLength);
  if RequiredTextLength <> 0 then
  begin
    ObtainedTextLength := SendMessage(Window, WM_GETTEXT, WParam(RequiredTextLength + 1), LParam(PChar(Result)));
    if ObtainedTextLength < RequiredTextLength then
      SetLength(Result, ObtainedTextLength);
  end;
end;

// RedirectFunction from the various Spring4D patch units:
procedure RedirectFunction(OrgProc, NewProc: Pointer);
type
  TJmpBuffer = packed record
    Jmp: Byte;
    Offset: Integer;
  end;
var
  n: UINT_PTR;
  JmpBuffer: TJmpBuffer;
begin
  JmpBuffer.Jmp := $E9;
  JmpBuffer.Offset := PByte(NewProc) - (PByte(OrgProc) + 5);
  if not WriteProcessMemory(GetCurrentProcess, OrgProc, @JmpBuffer, SizeOf(JmpBuffer), n) then
    RaiseLastOSError;
end;

function GetTextPatch(const Self: TSysControl): string;
begin
  Result := GetSysWindowText_WMGetText(Self.Handle);
end;

procedure PatchIt;
var
  ctx: TRttiContext;
  p: Pointer;
begin
  // Redirects the "getter" of TSysControl.Text into GetTextPatch:
  p := TRttiInstanceProperty(ctx.GetType(TSysControl).GetProperty('Text')).PropInfo.GetProc;
  RedirectFunction(p, @GetTextPatch);
end;

initialization

PatchIt;
{$ENDIF Skip_Patch_Vcl_Themes_GetSysWindowText}

end.
