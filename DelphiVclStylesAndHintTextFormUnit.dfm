object DelphiVclStylesAndHintTextForm: TDelphiVclStylesAndHintTextForm
  Left = 0
  Top = 0
  Caption = 'DelphiVclStylesAndHintTextForm'
  ClientHeight = 124
  ClientWidth = 316
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StyleRadioGroup: TRadioGroup
    Left = 0
    Top = 0
    Width = 145
    Height = 107
    Align = alLeft
    Caption = 'StyleRadioGroup'
    Items.Strings = (
      'Default system style'
      'First custom style')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    OnClick = StyleRadioGroupClick
  end
  object HintPanel: TPanel
    Left = 145
    Top = 0
    Width = 171
    Height = 107
    Align = alClient
    Caption = 'HintPanel'
    TabOrder = 1
    OnMouseMove = HintPanelMouseMove
  end
  object StaticText: TStaticText
    Left = 0
    Top = 107
    Width = 316
    Height = 17
    Align = alBottom
    Caption = 'StaticText'
    TabOrder = 2
    ExplicitWidth = 53
  end
  object BalloonHint: TBalloonHint
    Left = 160
    Top = 8
  end
end
