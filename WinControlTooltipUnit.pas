unit WinControlTooltipUnit; // originally ComponentBaloonHintU at https://stackoverflow.com/questions/26247528/displaying-x-icon-in-tballoonhint

// The adopted version:
// - also allows for non-balloon hint windows.
// - fixes a big limiting to ~700 characters

interface

uses
  Vcl.Controls,
  Winapi.CommCtrl,
  Vcl.Graphics;

{$SCOPEDENUMS ON}

type
  TIconKind = (None = TTI_NONE, Info = TTI_INFO, Warning = TTI_WARNING, Error = TTI_ERROR, Info_Large = TTI_INFO_LARGE, Warning_Large = TTI_WARNING_LARGE, Eror_Large = TTI_ERROR_LARGE);
  TWinControlBaloonhint = class helper for TWinControl
  public
    procedure ShowTooltip(const Icon: TIconKind; const Title, Text: string; const ShowAsBalloon: Boolean = True);
  end;

implementation
uses
  Windows;

{ TWinControlBaloonhint }

procedure TWinControlBaloonhint.ShowTooltip(const Icon: TIconKind; const Title, Text: string; const ShowAsBalloon:
    Boolean = True);
var
  hWndTip: THandle;
  ToolInfo: TToolInfo;
  BodyText: pWideChar;
  BodyTextLength: Integer;
  WindowStyle: Cardinal;
begin
  // Styles via https://msdn.microsoft.com/en-us/library/windows/desktop/bb760248.aspx
  WindowStyle := WS_POPUP or TTS_NOPREFIX or TTS_ALWAYSTIP;
  if ShowAsBalloon then
    WindowStyle := WindowStyle or TTS_CLOSE or TTS_BALLOON;

  hWndTip := CreateWindow(TOOLTIPS_CLASS, nil, WindowStyle, 0, 0, 0, 0, Handle, 0, HInstance, nil);

  if hWndTip = 0 then
    exit;

  BodyTextLength := 2 * Length(Text); // should be more like get the widechar byte count of a string, but this works for now
  GetMem(BodyText, BodyTextLength);

  try
    ToolInfo.cbSize := SizeOf(TToolInfo);
    ToolInfo.uFlags := TTF_CENTERTIP or TTF_TRANSPARENT or TTF_SUBCLASS;
    ToolInfo.hWnd := Handle;
    ToolInfo.lpszText := StringToWideChar(Text, BodyText, BodyTextLength);
    SetWindowPos(hWndTip, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOACTIVATE or SWP_NOMOVE or SWP_NOSIZE);
    ToolInfo.Rect := GetClientRect();

    SendMessage(hWndTip, TTM_ADDTOOL, 1, NativeInt(@ToolInfo));
    SendMessage(hWndTip, TTM_SETTITLE, NativeInt(Icon), NativeInt(PChar(Title)));
    SendMessage(hWndTip, TTM_TRACKACTIVATE, NativeInt(true), NativeInt(@ToolInfo));
  finally
    FreeMem(BodyText);
  end;
end;

end.
