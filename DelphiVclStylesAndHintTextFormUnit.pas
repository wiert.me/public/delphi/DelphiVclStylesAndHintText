unit DelphiVclStylesAndHintTextFormUnit;

interface

uses
  System.Classes,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.StdCtrls,
  Vcl.ExtCtrls;

type
  TDelphiVclStylesAndHintTextForm = class(TForm)
    StyleRadioGroup: TRadioGroup;
    HintPanel: TPanel;
    BalloonHint: TBalloonHint;
    StaticText: TStaticText;
    procedure HintPanelMouseMove(Sender: TObject; Shift: TShiftState; X, Y:
        Integer);
    procedure FormCreate(Sender: TObject);
    procedure StyleRadioGroupClick(Sender: TObject);
  private
    procedure SwitchToDefaultSystemStyle;
    procedure SwitchToFirstCustomStyle;
    procedure UpdateCaptionFromCurrentStyle;
  end;

var
  DelphiVclStylesAndHintTextForm: TDelphiVclStylesAndHintTextForm;

implementation

uses
  Vcl.Styles,
  Vcl.Themes,
  WinControlTooltipUnit;

{$R *.dfm}

procedure TDelphiVclStylesAndHintTextForm.HintPanelMouseMove(Sender:
    TObject; Shift: TShiftState; X, Y: Integer);
begin
//  BalloonHint.Style := TBalloonHintStyle.bhsStandard; // does not reproduce with VCL wrapper around non-Balloon hint window
//  BalloonHint.HideAfter := 5000;
//  BalloonHint.Title := StyleRadioGroup.Hint;
//  BalloonHint.ShowHint(Sender as TControl);

  (Sender as TWinControl).ShowTooltip(TIconKind.Info, StyleRadioGroup.Hint, StyleRadioGroup.Hint, False); // reproduces for non-balloon native hint windows.
end;

procedure TDelphiVclStylesAndHintTextForm.FormCreate(Sender: TObject);
var
  Character: Char;
  HintText: string;
begin
  for Character := 'A' to 'Z' do // 26 times 16 characters
    HintText := HintText + Character + '123456789ABCDEF ';
//  HintText := HintText + #13#10;
  for Character := 'a' to 'z' do // 26 times 16 characters
    HintText := HintText + Character + '123456789ABCDEF ';
  StyleRadioGroup.Hint := HintText;
  StaticText.Caption := HintText;
end;

procedure TDelphiVclStylesAndHintTextForm.StyleRadioGroupClick(Sender: TObject);
begin
  case StyleRadioGroup.ItemIndex of
    0: SwitchToDefaultSystemStyle();
    1: SwitchToFirstCustomStyle();
  end;
end;

procedure TDelphiVclStylesAndHintTextForm.SwitchToDefaultSystemStyle;
begin
  TStyleManager.SetStyle(TStyleManager.SystemStyle);
  UpdateCaptionFromCurrentStyle();
end;

procedure TDelphiVclStylesAndHintTextForm.SwitchToFirstCustomStyle;
var
  StyleNames: TArray<string>;
begin
  StyleNames := TStyleManager.StyleNames;
  TStyleManager.TrySetStyle(StyleNames[1]);
  UpdateCaptionFromCurrentStyle();
end;

procedure TDelphiVclStylesAndHintTextForm.UpdateCaptionFromCurrentStyle;
begin
  Caption := TStyleManager.ActiveStyle.Name;
end;

end.
